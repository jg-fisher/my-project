from flask import Flask, render_template, request
from services.nomics_api_service import NomicsApiService

# Creating an instance of the Flask object
app = Flask(__name__)

# defining and adding endpoints and accepted request methods for the Flask webserver
@app.route('/', methods=['GET', 'POST'])
def index():

    # if a post request to this '/' endpoint is made (where the data from the input box goes when you press submit)
    if request.method == 'POST':

        # extracts the text that is submitted from the input field
        currency_ticker = request.form['text']

        # Creating an instance of the NomicsApiService Class
        nomics_api_service = NomicsApiService()

        # Capitalize to format the cryptocurrency ticker as Nomics api expects
        currency_ticker = currency_ticker.upper()

        # Calls a method from the Nomics API class which hits the API and stores the response in data
        data = nomics_api_service.get(currency_ticker)

        # renders html template passing in the data from the exchange
        return(render_template('index.html', data=data))

    # renders html template for a GET request
    return render_template('index.html')

# if this file is the one you run (convention)
if __name__ == '__main__':
    # starts the Flask webserver
    app.run()