import requests

class NomicsApiService:
    """
    Initializes an instance of the NomicsApiService class.
    """
    def __init__(self):
        # base url for the Nomics APi endpoint, includes the API Key used to identify yourself to their API.
        self.url = 'https://api.nomics.com/v1/markets/prices?key=43265da203832ade9afac5e7d1333544'

    def get(self, currency_ticker):
        """
        Makes a GET request to the Nomics cryptocurrency API.
        """
        # formats the url string per the Nomics API documentation
        res = requests.get('{}&currency={}'.format(self.url, currency_ticker))
        # returns the response data that is received from the Nomics API
        return res.json()


